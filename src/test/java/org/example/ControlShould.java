package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.example.RoverShould.*;

public class ControlShould {

    CommandAndControl createCommandAndControl(Orientation orientation) {
        return new CommandAndControl(
                mars, atInitialPosition, orientation
        );
    }

    @Test
    @DisplayName("Should send move forward command ('f')")
    void test0() {
        CommandAndControl ground = createCommandAndControl(Orientation.South);
        ground.execute("f");
        assertThat(ground.position()).isEqualTo(atSouth);
    }

    @Test
    @DisplayName("Should send error if the command is unknown")
    void test1() {
        CommandAndControl ground = createCommandAndControl(Orientation.South);
        Status status = ground.execute("x");
        assertThat(status).isEqualTo(new Status(State.Failed, "The command 'x' is illegal"));
    }

    @Test
    @DisplayName("Should send move backward command ('b')")
    void test2() {
        CommandAndControl ground = createCommandAndControl(Orientation.South);
        ground.execute("b");
        assertThat(ground.position()).isEqualTo(atNorth);
    }

    @Test
    @DisplayName("Should send move turn left command ('l')")
    void test3() {
        CommandAndControl ground = createCommandAndControl(Orientation.North);
        ground.execute("lf");
        assertThat(ground.position()).isEqualTo(atWest);
    }

    @Test
    @DisplayName("Should send move turn right command ('r')")
    void test4() {
        CommandAndControl ground = createCommandAndControl(Orientation.North);
        ground.execute("rf");
        assertThat(ground.position()).isEqualTo(atEast);
    }

    @Test
    @DisplayName("Should return OK Status when the sequence command is completed")
    void test5() {
        CommandAndControl ground = createCommandAndControl(Orientation.North);
        Status status = ground.execute("fff");
        assertThat(status).isEqualTo(new Status(State.Completed));
    }

    @Test
    @DisplayName("Should return Stopped Status when the rover encountered an obstacle")
    void test6() {
        CommandAndControl ground = createCommandAndControl(Orientation.North);
        Status status = ground.execute("fff");
        assertThat(status).isEqualTo(new Status(State.Completed));
    }

    @Test
    @DisplayName("Should display the planet")
    void test7() {
        CommandAndControl ground = createCommandAndControl(Orientation.North);
        assertThat(ground.display()).isEqualTo("""
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫⬆️🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🪨🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
""");
        ground.execute("l");
        assertThat(ground.display()).isEqualTo("""
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫⬅️🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🪨🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
""");
        ground.execute("l");
        assertThat(ground.display()).isEqualTo("""
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫⬇️🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🪨🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
""");
        ground.execute("l");
        assertThat(ground.display()).isEqualTo("""
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫➡️🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🪨🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
""");
    }
}


