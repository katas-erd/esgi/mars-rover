package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class RoverShould {

    static Position atInitialPosition = new Position(5, 5);
    static Position atNorth = new Position(5, 4);
    static Position atSouth = new Position(5, 6);
    static Position atWest = new Position(4, 5);
    static Position atEast = new Position(6, 5);
    static Planet mars = new Planet(new Size(10, 10), Arrays.asList(new Position(8, 8)));

    @Test
    @DisplayName("land on mars")
    public void test0() {
        Rover marsRover = createAMarsRover( Orientation.South);

        Assertions.assertEquals(new Position(5, 5), marsRover.position());
    }

    @Test
    @DisplayName("move forward")
    public void test1() {
        makeTest(
                from(Orientation.North, atNorth),
                from(Orientation.South, atSouth),
                from(Orientation.East, atEast),
                from(Orientation.West, atWest)
        ).forEach(
                data -> {
                    Rover marsRover = createAMarsRover( data.orientation());
                    marsRover.moveForward();
                    Assertions.assertEquals(data.expected(), marsRover.position());
                }
        );
    }

    @Test
    @DisplayName("move backward")
    public void test2() {
        makeTest(
                from(Orientation.North, atSouth),
                from(Orientation.South, atNorth),
                from(Orientation.East, atWest),
                from(Orientation.West, atEast)
        ).forEach(
                data -> {
                    Rover marsRover = createAMarsRover( data.orientation());
                    marsRover.moveBackward();
                    Assertions.assertEquals(data.expected(), marsRover.position());
                }
        );
    }

    @Test
    @DisplayName("turn left")
    public void test3() {
        makeTest(
          from(Orientation.North, atWest),
                from(Orientation.South, atEast),
                from(Orientation.East, atNorth),
                from(Orientation.West, atSouth)
        ).forEach(
                data -> {
                    Rover marsRover = createAMarsRover( data.orientation());
                    marsRover.turn(Direction.Left);
                    marsRover.moveForward();
                    Assertions.assertEquals(data.expected(), marsRover.position());
                }
        );
    }

    @Test
    @DisplayName("turn right")
    public void test4() {
        makeTest(
                from(Orientation.North, atEast),
                from(Orientation.South, atWest),
                from(Orientation.East, atSouth),
                from(Orientation.West, atNorth)
        ).forEach(
                data -> {
                    Rover marsRover = createAMarsRover( data.orientation());
                    marsRover.turn(Direction.Right);
                    marsRover.moveForward();
                    Assertions.assertEquals(data.expected(), marsRover.position());
                }
        );
    }


    public static Rover createAMarsRover(Orientation facing) {
        return new Rover(new GPS(
                mars,
                atInitialPosition),
                facing);
    }


    List<MovingTestData> makeTest(MovingTestData... data) {
        return Arrays.asList(data);
    }
    MovingTestData from(Orientation orientation, Position position) {
        return new MovingTestData(orientation, position);
    }
}

record MovingTestData(Orientation orientation, Position expected) {}