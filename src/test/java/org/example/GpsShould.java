package org.example;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.example.RoverShould.mars;

public class GpsShould {




    GPS create() {
        return new GPS(mars, new Position(5, 5));
    }

    GPS create(Position initialPosition) {
        return new GPS(mars, initialPosition);
    }
    @Test
    @DisplayName("give the position at north")
    void test3() {
        GPS gps = create();
        gps.moveTo(Orientation.North);
        assertThat(gps.currentPosition()).isEqualTo(new Position(5, 4));
    }

    @Test
    @DisplayName("give the position at south")
    void test4() {
        GPS gps = create();
        gps.moveTo(Orientation.South);
        assertThat(gps.currentPosition()).isEqualTo(new Position(5, 6));
    }

    @Test
    @DisplayName("give the position at east")
    void test5() {
        GPS gps = create();
        gps.moveTo(Orientation.East);
        assertThat(gps.currentPosition()).isEqualTo(new Position(6, 5));
    }

    @Test
    @DisplayName("give the position at west")
    void test6() {
        GPS gps = create();
        gps.moveTo(Orientation.West);
        assertThat(gps.currentPosition()).isEqualTo(new Position(4, 5));
    }

    @Test
    @DisplayName("refuse to move if there is an obstacle in front of us")
    void test7() {
        GPS gps = create(new Position(8, 9));
        assertThat(gps.moveTo(Orientation.North)).isFalse();
        assertThat(gps.currentPosition()).isEqualTo(new Position(8, 9));
    }
}
