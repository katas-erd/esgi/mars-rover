package org.example;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.example.RoverShould.mars;

class PlanetShould {

    @Test
    @DisplayName("give the position at north")
    void test3() {
        assertThat(mars.positionAt(Orientation.North, new Position(5, 5))).isEqualTo(new Position(5, 4));
    }

    @Test
    @DisplayName("give the position at south")
    void test4() {
        assertThat(mars.positionAt(Orientation.South, new Position(5, 5))).isEqualTo(new Position(5, 6));
    }

    @Test
    @DisplayName("give the position at east")
    void test5() {
        assertThat(mars.positionAt(Orientation.East, new Position(5, 5))).isEqualTo(new Position(6, 5));
    }

    @Test
    @DisplayName("give the position at west")
    void test6() {
        assertThat(mars.positionAt(Orientation.West, new Position(5, 5))).isEqualTo(new Position(4, 5));
    }

    @Test
    @DisplayName("give the position at WEST when at the West edge of the grid")
    void test7() {
        assertThat(mars.positionAt(Orientation.West, new Position(0, 5))).isEqualTo(new Position(9, 5));
    }

    @Test
    @DisplayName("give the position at EAST when at the East edge of the grid")
    void test8() {
        assertThat(mars.positionAt(Orientation.East, new Position(9, 5))).isEqualTo(new Position(0, 5));
    }

    @Test
    @DisplayName("give the position at NORTH when at the North edge of the grid")
    void test9() {
        assertThat(mars.positionAt(Orientation.North, new Position(5, 0))).isEqualTo(new Position(5, 9));
    }

    @Test
    @DisplayName("give the position at SOUTH when at the South edge of the grid")
    void test10() {
        assertThat(mars.positionAt(Orientation.South, new Position(5, 9))).isEqualTo(new Position(5, 0));
    }

    @Test
    @DisplayName("return true when there is an obstacle at the given position")
    void test11() {
        assertThat(mars.hasObstable(new Position(6, 6))).isFalse();
        assertThat(mars.hasObstable(new Position(8, 8))).isTrue();
    }

}
