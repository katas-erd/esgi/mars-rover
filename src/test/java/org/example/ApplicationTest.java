package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.example.RoverShould.createAMarsRover;
import static org.example.RoverShould.mars;

public class ApplicationTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @Test
    void test0() {
        provideInputs("10;10", "8;8", "5;5", "North", "flfrfr");
        Main.main(new String[] {});
        assertThat(outContent.toString().trim()).endsWith("""
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫⬆️🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫
🟫🟫🟫🟫🟫🟫🟫🟫🪨🟫
🟫🟫🟫🟫🟫🟫🟫🟫🟫🟫""");
    }


    void provideInputs(String... data) {
        String inputs = "";
        for(String d: data) {
            inputs += d + System.lineSeparator();
        }
        System.setIn(new ByteArrayInputStream(inputs.getBytes()));
    }
}
