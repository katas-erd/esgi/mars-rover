package org.example;

import java.util.List;


record Size(int width, int height) {}
class Planet {
    Size size;

    List<Position> obstacles;

    public Planet(Size size, List<Position> obstacles) {
        this.size = size;
        this.obstacles = obstacles;
    }

    Position positionAt(Orientation orientation, Position of) {
        Position newPosition = null;
        switch (orientation) {
            case North -> newPosition = of.plus(new Distance(0, -1));
            case South -> newPosition = of.plus(new Distance(0, 1));
            case East -> newPosition = of.plus(new Distance(1, 0));
            case West -> newPosition = of.plus(new Distance(-1, 0));
        }
        return normalize(newPosition);
    }


    Position normalize(Position reference) {
        return new Position(
                bound(reference.x(), size.width()),
                bound(reference.y(), size.height())
        );
    }

    int bound(int value, int max) {
        if (value < 0) {
            return max - (Math.abs(value) % max);
        }
        return value % max;
    }

    public boolean hasObstable(Position position) {
        return obstacles.contains(position);
    }

    public int width() {
        return size.width();
    }

    public int height() {
        return size.height();
    }
}
