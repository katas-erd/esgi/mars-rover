package org.example;

enum Orientation {
    North,
    South,
    East,
    West,
    ;

    public Orientation opposite() {
        return switch (this) {
            case North -> South;
            case South -> North;
            case East -> West;
            case West -> East;
        };
    }

    public Orientation left() {
        return switch (this) {
            case North -> West;
            case West -> South;
            case South -> East;
            case East -> North;
        };
    }

    public Orientation right() {
        return switch (this) {
            case North -> East;
            case East -> South;
            case South -> West;
            case West -> North;
        };
    }
}
