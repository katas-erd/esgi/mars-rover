package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    static Size toSize(String s) {
        String[] values = s.split(";");
        int w = Integer.parseInt(values[0].trim());
        int h = Integer.parseInt(values[0].trim());
        return new Size(w, h);
    }

    static Position toPosition(String s) {
        String[] values = s.split(";");
        int x = Integer.parseInt(values[0].trim());
        int y = Integer.parseInt(values[0].trim());
        return new Position(x, y);
    }

    static List<Position> toPositionList(String s) {
        String[] values = s.split(",");
        List<Position> positions = new ArrayList<>();
        for (String value : values) {
            positions.add(toPosition(value));
        }
        return positions;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Size of the planet (width;height): ");
        Size size = toSize(scanner.next());
        System.out.println("obstacles (x;y,x;y,x;y): ");
        List<Position> obstacles = toPositionList(scanner.next());
        System.out.println("Landing position (x;y): ");
        Position landingPosition = toPosition(scanner.next());

        String input = scanner.next().trim();
        Orientation orientation = Orientation.valueOf(input);
        String commands = scanner.nextLine();

        CommandAndControl controller = new CommandAndControl(
                new Planet(size, obstacles),
                landingPosition, orientation);

        controller.execute(commands);

        System.out.println(controller.display());
    }
}
