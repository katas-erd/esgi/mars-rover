package org.example;

class Rover {
    final GPS gps;
    Orientation compas;

    Rover(GPS gps, Orientation facing) {
        this.gps = gps;
        this.compas = facing;
    }

    Position position() {
        return gps.currentPosition();
    }

    public void moveForward() {
        gps.moveTo(compas);
    }

    public void moveBackward() {
        gps.moveTo(compas.opposite());
    }

    public void turn(Direction direction) {
        switch (direction) {
            case Left -> compas = compas.left();
            case Right -> compas = compas.right();
        }
    }
}
