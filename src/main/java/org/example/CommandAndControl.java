package org.example;

class CommandAndControl {
    Rover rover;
    Planet planet;

    public CommandAndControl(Planet planet, Position landingPosition, Orientation landingOrientation) {
        this.rover = new Rover(new GPS(planet, landingPosition), landingOrientation);
        this.planet = planet;
    }

    Status execute(String commands) {
        String normalizedCommands = commands.trim().toLowerCase();
        for (int i = 0; i < normalizedCommands.length(); i++) {
            try {
                execute(normalizedCommands.charAt(i));
            } catch (IllegalArgumentException ex) {
                return new Status(State.Failed, ex.getMessage());
            }
        }

        return new Status(State.Completed);
    }

    void execute(char command) {
        switch (command) {
            case 'f':
                rover.moveForward();
                break;
            case 'b':
                rover.moveBackward();
                break;
            case 'l':
                rover.turn(Direction.Left);
                break;
            case 'r':
                rover.turn(Direction.Right);
                break;
            default:
                throw new IllegalArgumentException("The command '" + command + "' is illegal");
        }
    }

    String display() {
        String content = "";
        for (int y = 0; y != planet.height(); ++y) {
            for (int x = 0; x != planet.width(); ++x) {
                Position position = new Position(x, y);
                if (rover.position().equals(position)) {
                    switch (rover.compas) {
                        case North -> content += "⬆️";
                        case South -> content += "⬇️";
                        case East -> content += "➡️";
                        case West -> content += "⬅️";
                    }
                } else if (planet.hasObstable(position)) {
                    content += "🪨";
                } else {
                    content += "🟫";
                }
            }
            content += System.lineSeparator();
        }
        return content;
    }

    public Position position() {
        return rover.position();
    }
}
