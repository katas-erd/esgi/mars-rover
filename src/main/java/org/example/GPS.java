package org.example;

class GPS {
    final Planet planet;
    Position current;

    GPS(Planet landingOn, Position at) {
        planet = landingOn;
        current = at;
    }

    public Position currentPosition() {
        return current;
    }

    public boolean moveTo(Orientation orientation) {
        Position nextPosition = planet.positionAt(orientation, current);
        if (planet.hasObstable(nextPosition)) {
            return false;
        }
        current = nextPosition;
        return true;
    }
}
