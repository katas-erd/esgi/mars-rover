package org.example;

import org.example.Distance;

record Position(int x, int y) {
    public Position plus(Distance distance) {
        return new Position(x + distance.dx(), y + distance.dy());
    }
}
/*class Position {
    int x;
    int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position plus(org.example.Distance distance) {
        return new Position(x + distance.dx(), y + distance.dy());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }
}*/

















